/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simonwoodworth.mavenwebapp;

/**
 *
 * @author simon
 */
public class Calculator {
    
    public static double addThree(double a, double b, double c) {
        
        return a + b + c;
        
    }
    
    public static double multiplyThree(double a, double b, double c) {
        
        return a * b * c;
        
    }
    
    public static double maxThree(double a, double b, double c) {
        
        double max = a;
        
        if (b > max) max = b;
        if (c > max) max = c;

        return max;
    }
    
    public static double minThree(double a, double b, double c) {
        
        double min = a;
        
        if (b < min) min = b;
        if (c < min) min = c;

        return min;
    }
    
    public static double avgThree(double a, double b, double c) {

        return (a + b + c) / 3;
        
    }
    
}
